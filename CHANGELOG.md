# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]

	- No further feature planned.



## [0.3.0] - 2020-07-19
### Added

	- Ice monsters.

### Changed

	- Code refresh.
	- Obsidian sword recipe now requires two obsidian shards.
	- Snow walkers will spawn ice monsters o a chance of 1 in 6.



## [0.2.1] - 2019-11-28
### Changed

	- Minor code improvements.

### Removed

	- Support for MT v0.4.x



## [0.2.0] - 2019-09-28
### Added

	- Support for localization

### Modified

	- Random appearence moved to "after_activate" to avoid "on_activate" crash.
	- Obsidian Sword's alias set to "obsidian_sword"; e.g. /giveme obsidian_sword
	- License changed to E.U.P.L. v1.2


## [0.1.3] - 2018-06-11
## Modified

	- Armours' tables moved in the random appearence function to save memory.

## Added

	- Support for mobs_humans


## [0.1.2] - 2018-05-22
### Modified

	- Source code rewritten from scratch.
	- Mod rearranged to follow Minetest-Mods manifesto's guidelines.
	- Changed multimedia files' licenses back to their original ones.

### Removed

	- ../doc/


## [0.1.1] - 2018-04-07
### Added

- Folder "doc"
- Italian documentation
- Changelog


### Changed

- Documentation files have been moved into the aforementioned folder.
- Documentation files now show the link for the wiki.
