### MOBS OTHERS
![Mobs Others' screenshot](screenshot.png)  
**_Adds the Snow Walkers mobs, and an obsidian sword._**

**Version:** 0.3.0  
**Source code's license:** [EUPL v1.2][1] or later.  
**Multimedia files' license:** [LGPL v2.1][2] - [CC BY-SA v4.0 International][3]

**Dependencies:** default (found in [Minetest Game][4], mobs ([Mobs Redo][5])


**Obsidian Sword's recipe:**

O = obsidian shard, S = stick

O  
O  
S


### Installation

Unzip the archive, rename the folder to mobs_others and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods


[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html
[3]: https://creativecommons.org/licenses/by-sa/4.0/
[4]: https://github.com/minetest/minetest_game
[5]: https://forum.minetest.net/viewtopic.php?t=9917
